import { NestedError } from "./nested-error";

export class UserError extends NestedError {}
