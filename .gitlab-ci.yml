image: node:latest

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml

stages:
  - prepare
  - build
  - test
  - compatibility
  - release
  - postrelease

NPM:
  stage: prepare
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-npm
    paths:
      - node_modules/
    reports:
      dependency_scanning: gl-dependency-scanning.json
  script:
    - node --version
    - npm --version
    - npm ci
    - npm audit --json --production | npx gitlab-npm-audit-parser -o gl-dependency-scanning.json || true

Build:
  stage: build
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-build
    paths:
      - dist/
  script:
    - npm run --if-present build
    - npm pack
    - npm exec npm-pkg-lint

pages:
  stage: build
  artifacts:
    paths:
      - public
  only:
    - tags
    - triggers
  script:
    - npm run --if-present build
    - npm run build:docs

Changelog:
  stage: test
  needs: ["NPM"]
  dependencies: ["NPM"]
  script:
    - npm exec commitlint -- --from=origin/master --to=${CI_COMMIT_SHA}

ESLint:
  stage: test
  needs: ["NPM"]
  script:
    - npm run eslint -- --max-warnings 0

Jest:
  stage: test
  dependencies: ["NPM"]
  needs: ["NPM"]
  coverage: /Branches\s+:\s(\d+.\d+%)/
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-coverage
    paths:
      - coverage
    reports:
      junit: temp/jest.xml
  script:
    - npm test -- src/ elements/ tests/

Prettier:
  stage: test
  dependencies: ["NPM"]
  script:
    - npm run prettier:check

Docs:
  stage: test
  artifacts:
    reports:
      junit: temp/jest.xml
  script:
    - npm run build:docs
    - npm run htmlvalidate -- --config docs/htmlvalidate-templates.json 'docs/**/*.html'
    - npm run htmlvalidate -- --config docs/htmlvalidate-public.json 'public/**/*.html'
    - npm test -- docs
    - git status
    - test -z "$(git status --porcelain)" || (echo 'working copy dirty, need to commit updated specs'; exit 1)

.compat: &compat
  stage: compatibility
  dependencies: []
  needs: ["Build"]
  before_script:
    - npm ci
  script:
    - npm test
    - npm run --if-present build
    - npm run compatibility

Node 10.x (LTS):
  <<: *compat
  image: node:10

Node 12.x (LTS):
  <<: *compat
  image: node:12

Node 14.x (LTS):
  <<: *compat
  image: node:14

Node 15.x (current):
  <<: *compat
  image: node:15

Release:
  stage: release
  only:
    - web
  variables:
    GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
    GIT_AUTHOR_EMAIL: ${GITLAB_USER_EMAIL}
    GIT_COMMITTER_NAME: ${HTML_VALIDATE_BOT_NAME}
    GIT_COMMITTER_EMAIL: ${HTML_VALIDATE_BOT_EMAIL}
  script:
    - npm exec semantic-release

.downstream: &downstream
  stage: postrelease
  only:
    - tags
  variables:
    UPSTREAM_VERSION: "${CI_COMMIT_TAG}"

cypress-html-validate:
  <<: *downstream
  trigger: html-validate/cypress-html-validate

example-plugin:
  <<: *downstream
  trigger: html-validate/example-plugin

grunt-html-validate:
  <<: *downstream
  trigger: html-validate/grunt-html-validate

html-validate-angular:
  <<: *downstream
  trigger: html-validate/html-validate-angular

html-validate-jest-snapshot:
  <<: *downstream
  trigger: html-validate/html-validate-jest-snapshot

html-validate-vue:
  <<: *downstream
  trigger: html-validate/html-validate-vue

html-validate-vue-webpack-plugin:
  <<: *downstream
  trigger: html-validate/html-validate-vue

protractor-html-validate:
  <<: *downstream
  trigger: html-validate/protractor-html-validate

vue-cli-plugin-html-validate:
  <<: *downstream
  trigger: html-validate/vue-cli-plugin-html-validate
